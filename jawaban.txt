
1. membuat database
membuat database dengan nama “myshop”
 create database myshop;
 use myshop;
2.membuat table di dalam database
a. membuat table categories
 create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key (id)
    -> );
b. membuat table items
 create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar (255),
    -> price int(8),
    -> stock int(8),
    -> category_id int(8),
    -> primary key (id),
    -> foreign key (category_id) references categories(id)
    -> );
c. membuat table users
  create table users(
    -> id int(8) auto_increment
    -> ,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key (id)
    -> );
3. memasukan data pada table
a. data table categories
 insert into categories (name) value ("gadget"),("cloth"),("men"),("women"),("branded");
b. data pada table items
 insert into items(name,description,price,stock,category_id) value 
 ("sumsang b50","hape keren dari merek sumsang","4000000","100",1),
 ("Uniklooh","baju keren dari brand ternama","500000","50",2),
 ("IMHO Watch","jam tangan anak yang jujur banget","2000000","10",1);
c. data pada table users
 insert into users (name,email,password) value ("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
4. mengambil data dari database
a. mengambil data users kecuali password
 select id,name,email from users;
b. mengambil data items
 -query data items
  select * from items where price >1000000;
 -query data items
 select * from items where name like "uniklo%";
c.menampilkan data items join dengan kategori
  select items.id,items.name,items.description,items.price,items.stock,items.category_id,categories.name 
  as kategori from items left join categories on items.category_id=categories.id;
5. mengubah data dari database
 update items set price=2500000 where id=1;
 
 

 